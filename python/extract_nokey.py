#!/usr/bin/env python3
import sys

"""
Parsing data used during 'evaluate'.
See evaluate comments for more information.
"""
class ParsingData:
    def __init__(self, nokey_state):
        self.nokey_state = nokey_state  # Current state of the parser
        self.hist = dict()              # Key histogram of the current block
        self.guess = ""                 # Guessed key presses
        self.guess_alt = ""             # Alternative guessed key presses
        self.nokey_number = 0           # Number of consecutives NOKEY
        self.nokey_last_view = 0        # Number of key since we found a NOKEY
        self.last_nokey = 0             # Number of consecutives NOKEY (2) FIXME
        self.history = []               # History of KEY parsed


"""
Prettify the string of special key:
ENTER, SPACE, CTRL, SUPPR, SHIFT

[in]key: string representing the key
[out]: string of key prettified
"""
def prettify_key(key):
    if key == 'ENTER':
        key = '¬'
    elif key == 'SPACE':
        key = ' '
    elif key == 'CTRL':
        key = '©'
    elif key == 'SUPPR':
        key = '«'
    elif key == 'SHIFT':
        key = '↑'

    return key


"""
Handle parsing routine when entering in NOKEY state.

[in]data: ParsingData object
"""
def entering_nokey_state(data):
    data.nokey_state = True
    data.nokey_last_view = 0

    # We delete 5 previous keys that was part of nokey_state
    for prev_key in data.history[-5:]:
        if prev_key in data.hist:
            data.hist[prev_key] -= 1

    # Check if dictionary is empty, should not happen
    if not data.hist:
        return

    # Guess key for this block is max of histogram
    # guessed_key = max(data.hist, key=data.hist.get)
    sorted_hist = {key: value for key, value in sorted(data.hist.items(), key=lambda item: item[1])}
    guessed_key = list(sorted_hist.keys())[-1]
    alt_guessed = list(sorted_hist.keys())[-2]

    # We print some debug
    print(data.hist)
    print("GUESSED " + str(guessed_key))
    print("ALT     " + str(alt_guessed) + "\n")

    # Add in guess string and reset the histogram dictionnary
    guessed_key = prettify_key(guessed_key)
    alt_guessed = prettify_key(alt_guessed)
    data.guess += guessed_key
    data.guess_alt += alt_guessed
    data.hist.clear()


"""
Handle parsing routine when being in NOKEY state.

[in]data: ParsingData object
[in]key: Current key being parsed
"""
def handle_nokey_state(data, key):
    # We test if we leaving NOKEY state
    if data.nokey_last_view > 3:
        data.nokey_state = False
        data.nokey_number = 0
        # We reset the dictionnary
        data.hist.clear()

        # We restore the 3 previous keys found discarded since we
        # though that they were in NOKEY state.
        for prev_key in data.history[-3:]:
            if prev_key in data.hist:
                data.hist[prev_key] += 1
            else:
                data.hist[prev_key] = 0

    # We are in NOKEY state, we fill history and check NOKEY
    else:
        if key == 'NOKEY':
            data.nokey_last_view = 0
        else:
            data.history.append(key)
            data.nokey_last_view += 1



"""
Handle parsing routine when NOT being in NOKEY state.

[in]data: ParsingData object
[in]key: Current key being parsed
"""
def handle_key_state(data, key):
    if key == 'NOKEY':
        if data.last_nokey < 2:
            data.nokey_number += 1
            data.last_nokey = 0
        else:
            data.nokey_number = 1
            data.last_nokey = 0
    else:
        data.last_nokey += 1
        data.history.append(key)
        if key in data.hist:
            data.hist[key] += 1
        else:
            data.hist[key] = 0


"""
Take a list of keys and return guessed text.
This method use NOKEY block to assume the separation between the key pressed.
NOKEY state is the parsing state between 2 keys.

Example:
List : [A, B, A, A, NOKEY, NOKEY, D, NOKEY, C, D, D, D]
Block: |_____1_____|__________2___________|_____3_____|

We consider block 1 is a key press, and its result is the key that appears the most.
We consider block 2 as the time between 2 key press and want to discard it.
We consider block 3 the same way as block 1.

For doing it we consider entering NOKEY state when finding many successive or quasi
successive NOKEY. We consider leaving NOKEY state when we are not finding successive
or quasy successive NOKEY anymore.

[in]result: list of string keys
[out]: string of guessed text
"""
def evaluate(keys):
    # Create data structure, we assume beginning in NOKEY state
    data = ParsingData(False)

    for key in keys:
        # We are entering NOKEY state, save previous key guess
        if data.nokey_number > 4 and not data.nokey_state:
            entering_nokey_state(data)

        # We are in NOKEY state
        elif data.nokey_state:
            handle_nokey_state(data, key)

        # We are not in NOKEY state
        else:
            handle_key_state(data, key)

    return data.guess, data.guess_alt


if __name__ == "__main__":

    input_file = 'result.txt'
    if len(sys.argv) == 2:
        input_file = sys.argv[1]

    result = [word.split(',') for word in open(input_file, 'r').readlines()][0]
    guess, guess_alt = evaluate(result)
    print("Result:      " + guess)
    print("Alternative: " + guess_alt)
