#!/usr/bin/env python3

import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from joblib import parallel_backend


"""
Code from given file read_pics.py
"""
def read_int(f):
    ba = bytearray(4)
    f.readinto(ba)
    prm = np.frombuffer(ba, dtype=np.int32)
    return prm[0]


def read_double(f):
    ba = bytearray(8)
    f.readinto(ba)
    prm = np.frombuffer(ba, dtype=np.double)
    return prm[0]


def read_double_tab(f, n):
    ba = bytearray(8*n)
    nr = f.readinto(ba)
    if nr != len(ba):
        return []
    else:
        prm = np.frombuffer(ba, dtype=np.double)
        return prm


def get_pics_from_file(filename):
    # Lecture du fichier d'infos + pics detectes (post-processing KeyFinder)
    print("Ouverture du fichier de pics "+filename)
    f_pic = open(filename, "rb")
    info = dict()
    info["nb_pics"] = read_int(f_pic)
    #print("Nb pics par trame: " + str(info["nb_pics"]))
    info["freq_sampling_khz"] = read_double(f_pic)
    #print("Frequence d'echantillonnage: " + str(info["freq_sampling_khz"]) + " kHz")
    info["freq_trame_hz"] = read_double(f_pic)
    #print("Frequence trame: " + str(info["freq_trame_hz"]) + " Hz")
    info["freq_pic_khz"] = read_double(f_pic)
    #print("Frequence pic: " + str(info["freq_pic_khz"]) + " kHz")
    info["norm_fact"] = read_double(f_pic)
    #print("Facteur de normalisation: " + str(info["norm_fact"]))
    tab_pics = []
    pics = read_double_tab(f_pic, info["nb_pics"])
    nb_trames = 1
    while len(pics) > 0:
        nb_trames = nb_trames+1
        tab_pics.append(pics)
        pics = read_double_tab(f_pic, info["nb_pics"])
    info["nb_frame"] = nb_trames
    #print("Nb trames: " + str(nb_trames))
    f_pic.close()
    return tab_pics, info
"""
End of given file
"""


def get_all_letter(char):
    all_pic = []
    for i in range(65, 91):
        file = "../data/pics_" + chr(i) + ".bin"
        pics, info = get_pics_from_file(file)
        for k in pics:
            all_pic.append(k)
        for j in range(info["nb_frame"] - 1):
            char.append(chr(i))

    for i in range(48, 58):
        file = "../data/pics_" + chr(i) + ".bin"
        pics, info = get_pics_from_file(file)
        for k in pics:
            all_pic.append(k)
        for j in range(info["nb_frame"] - 1):
            char.append(chr(i))
    return all_pic, char


def training():
    print("Start training")
    char = []
    X, y = get_all_letter(char)

    button = ['CTRL', 'ENTER', 'NOKEY', 'SHIFT', 'SPACE', 'SUPPR']

    for i in button:
        pics_button, info = get_pics_from_file("../data/pics_" + i + ".bin")
        for k in pics_button:
            X.append(k)
        for j in range(info["nb_frame"] - 1):
            y.append(i)

    print(len(X))
    print(len(y))

    with parallel_backend('threading', n_jobs=2):
        X_train, X_test, y_train, y_test = train_test_split(X, y)
        clf = MLPClassifier(random_state=1, max_iter=300)

    clf.fit(X_train, y_train)

    print("end training")
    return clf


if __name__ == "__main__":
    # Train the model
    clf = training()

    # Retrieve LOGINMDP and predict keys
    pics_login, info = get_pics_from_file("../data/pics_LOGINMDP.bin")
    result = clf.predict(pics_login)

    # Output prediction in file
    output = open('result.txt', 'w')
    print(*result, sep=",", file = output)
    output.close()
