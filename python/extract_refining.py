import sys


"""
Prettify the string of special key:
ENTER, SPACE, CTRL, SUPPR, SHIFT

[in]key: string representing the key
[out]: string of key prettified
"""
def prettify_key(key):
    if key == 'ENTER':
        key = '¬'
    elif key == 'SPACE':
        key = ' '
    elif key == 'CTRL':
        key = '©'
    elif key == 'SUPPR':
        key = '«'
    elif key == 'SHIFT':
        key = '↑'
    return key

"""
Take a list and regroup the same value if they are sibling 

Example:
    List : [A, A, A, A, NOKEY, NOKEY, NOKEY, C, D, D, D]
    Result: [A, NOKEY, C, D]
    
[in]lst: the list to merge
[out] the same list after merging
"""
def delete_duplication(lst):
    obj = lst[0]
    tmp = [obj]
    for i in range(1, len(lst)):
        if lst[i] != obj:
            tmp.append(lst[i])
        obj = lst[i]
    return tmp

"""
Take a list and delete all the NOKEY value

[in]lst: the list to modify
[out]: the list without the NOKEY
"""
def delete_nokey(lst):
    tmp = []
    for i in lst:
        if (i != "NOKEY"):
            tmp.append(i)
    return tmp


"""
Take a list of char and create a string from these char 
(using the prettify for the key button)

[in]lst: the list to transform
[out]: the final string
"""
def list_to_string(lst):
    button = ['CTRL', 'ENTER', 'NOKEY', 'SHIFT', 'SPACE', 'SUPPR']
    s = ""
    for word in lst:
        if not word in button:
            s += word
        else:
            s += prettify_key(word)
    return s
        
"""
Create a list and add all the value of the 
list in parameter if the value is equal to 
its left neighbor

Example:
    List : [A, A, A, A, NOKEY, NOKEY, NOKEY, C, D, D, D]
    Result: [A, A, A, NOKEY, NOKEY, D, D, D]
    
[in]lst: the original list
[out] the new list after modifications 
"""
def delete_alone(lst):
    result = []
    for i in range(len(lst) - 1):
        if lst[i] == lst[i + 1]:
            result.append(lst[i])
    return result

"""
Take the list of frame and return
the final result as a string

[in]result: the list of frame
[in]precision: the int parameter for the delete_alone function
[out] the final result (string)
"""
def evaluate(result, precision):
    for i in range(precision):
        result = delete_alone(result)
    result = delete_duplication(result)
    result = delete_nokey(result)
    result_string = list_to_string(result)
    return result_string

if __name__ == "__main__":

    input_file = 'result.txt'
    if len(sys.argv) == 2:
        input_file = sys.argv[1]
    result = [word.split(',') for word in open(input_file, 'r').readlines()][0]
    guess = evaluate(result, 5)
    print("result: " + guess)
